// Based on a script by Kathie Decora : katydecorah.com/code/lunr-and-jekyll/

// Create the lunr index for the search
var index = elasticlunr(function () {
  this.addField('title')
  this.addField('author')
  this.addField('layout')
  this.addField('content')
  this.setRef('id')
});

// Add to this index the proper metadata from the Jekyll content

index.addDoc({
  title: "404: Page not found",
  author: null,
  layout: "page",
  content: "Sorry, we've misplaced that URL or it's pointing to something that doesn't exist.\nHead back home to try finding it again.\n",
  id: 0
});
index.addDoc({
<<<<<<< HEAD
=======
  title: "Mateo-A Caballero",
  author: null,
  layout: "page",
  content: "\norcid\n\n \n\nBiografía\nMateo Caballero Cantor (Bogotá, 1997). Romántico explorador del desorden bogotano,\n    es un escritor y fotógrafo de 24 años, quien considera que el arte se construye en\n    comunidad. Es actual estudiante del pregrado de Creación Literaria de la universidad\n    Central y creador de la Revista digital Pequeños Relatos. Participó en el encuentro de \n    Poesía y la Palabra del municipio de Tenjo. Integró la antología 21 N – 100 autores, 100 \n    relatos, 100 palabras – tapa azul, editorial ITA, con el poema Días de revolución. Con sus \n    escritos ha colaborado para algunas revistas nacionales.\nEl papel del editor es el más importante en el proceso de elaboración de una obra literaria\n    para que llegue a su mejor versión. El editor no es un simple retorico maniqueísta, es más \n    que eso, es la puerta para que el autor detecte todas las zonas de mejoras. Sin editores\n    nos ahogaríamos en océanos de literatura muerta, pues no habría la calidad que requiere\n    una obra de arte.\nIdiomas\n  \n     Español    \n  \n\n\nIntereses literarios\nLiteratura latinoamericana, poesía colombiana, ensayos literarios y cuentos de género negro\n\n",
  id: 1
});
index.addDoc({
>>>>>>> 4baaaf547d630beb6b27e7800296c55f150d90aa
  title: null,
  author: null,
  layout: "default",
  content: "\n  hello world!\n\n",
<<<<<<< HEAD
  id: 1
=======
  id: 2
>>>>>>> 4baaaf547d630beb6b27e7800296c55f150d90aa
});
index.addDoc({
  title: "Search",
  author: null,
  layout: "page",
  content: "\nThis is a simple search system. It will match most non-grammatical words.\n  Not all results will be highlighted on the page.\n\n\n\n\n\n\n\n",
<<<<<<< HEAD
  id: 2
=======
  id: 3
>>>>>>> 4baaaf547d630beb6b27e7800296c55f150d90aa
});
index.addDoc({
  title: null,
  author: null,
  layout: null,
  content: "// Based on a script by Kathie Decora : katydecorah.com/code/lunr-and-jekyll/\n\n// Create the lunr index for the search\nvar index = elasticlunr(function () {\n  this.addField('title')\n  this.addField('author')\n  this.addField('layout')\n  this.addField('content')\n  this.setRef('id')\n});\n\n// Add to this index the proper metadata from the Jekyll content\n{% assign count = 0 %}{% for text in site.pages %}\nindex.addDoc({\n  title: {{text.title | jsonify}},\n  author: {{text.author | jsonify}},\n  layout: {{text.layout | jsonify}},\n  content: {{text.content | jsonify | strip_html}},\n  id: {{count}}\n});{% assign count = count | plus: 1 %}{% endfor %}\n\n// Builds reference data (maybe not necessary for us, to check)\nvar store = [{% for text in site.pages %}{\n  \"title\": {{text.title | jsonify}},\n  \"author\": {{text.author | jsonify}},\n  \"layout\": {{ text.layout | jsonify }},\n  \"link\": {{text.url | jsonify}},\n}\n{% unless forloop.last %},{% endunless %}{% endfor %}]\n\n// Query\nvar qd = {}; // Gets values from the URL\nlocation.search.substr(1).split(\"&\").forEach(function(item) {\n    var s = item.split(\"=\"),\n        k = s[0],\n        v = s[1] && decodeURIComponent(s[1]);\n    (k in qd) ? qd[k].push(v) : qd[k] = [v]\n});\n\nfunction doSearch() {\n  var resultdiv = document.querySelector('#results');\n  var query = document.querySelector('input#search').value;\n\n  // The search is then launched on the index built with Lunr\n  var result = index.search(query);\n  resultdiv.innerHTML = \"\";\n  if (result.length == 0) {    \n    resultdiv.append(document.createElement('p').innerHTML = 'No results found.');\n  } else if (result.length == 1) {\n    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' result');\n  } else {\n    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' results');\n  }\n  // Loop through, match, and add results\n  for (var item in result) {\n    var ref = result[item].ref;\n    res = document.createElement('div')\n    res.classList.add(\"result\")\n    link = document.createElement('a')\n    link.setAttribute('href', '{{site.baseurl}}'+store[ref].link+'?q='+query)\n    link.innerHTML = store[ref].title || 'Untitled page'\n    p = document.createElement('p')\n    p.appendChild(link)\n    res.appendChild(p)\n    resultdiv.appendChild(res)\n  }\n}\n\nvar callback = function(){\n  searchInput = document.querySelector('input#search') \n  if (qd.q) {\n    searchInput.value = qd.q[0];\n    doSearch();\n  }\n  searchInput.addEventListener('keyup', doSearch);\n};\n\nif (\n    document.readyState === \"complete\" ||\n    (document.readyState !== \"loading\" && !document.documentElement.doScroll)\n) {\n  callback();\n} else {\n  document.addEventListener(\"DOMContentLoaded\", callback);\n}\n",
<<<<<<< HEAD
  id: 3
=======
  id: 4
>>>>>>> 4baaaf547d630beb6b27e7800296c55f150d90aa
});
index.addDoc({
  title: null,
  author: null,
  layout: null,
  content: "/*\n  Common Variables\n\n  Feel free to change!\n*/\n\n/* Fonts */\n$main-font: \"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;\n$heading-font: sans-serif;\n$regular-font-size: 1.25em; /* 20px / 16px = 1.25em; support text resizing in all browsers */\n\n\n/*\n  Color\n\n  Make sure to leave color-scheme in `_config.yml` file empty for granular control\n*/\n\n$text-color: #454545;\n$heading-color: #404040;\n$link-color: #841212;\n\n@import \"ed\";\n@import \"syntax\";\n@import \"CETEIcean.css\";\n",
<<<<<<< HEAD
  id: 4
=======
  id: 5
>>>>>>> 4baaaf547d630beb6b27e7800296c55f150d90aa
});
index.addDoc({
  title: "Valentina Osorio-Beltran",
  author: null,
  layout: "page",
  content: "\nBiografía\nLink ORCID\nLink Google Scholar\n\n\n\nSoy estudiante del pregrado en literatura de la Universidad EAFIT. Graduada de la Institución Educativa Madre María Mazzarello en el año 2017. Hago parte del semillero de investigación Acústica, que hace parte del pregrado de Comunicación Social. Actualmente hago una publicación periódica de un podcast junto a la emisora universitaria. \n\nIntereses Académicos\nMe interesan las humanidades y su posicionamiento en la actualidad, cómo comulgan con fenómenos como las redes sociales, las situaciones políticas y la actualidad social. También tengo un interés particular por la literatura en contexto del conflicto armado, tanto colombiana como de otros lugares.\n\nIdiomas\n\n    Español (lengua materna)\n    Inglés (nivel B2)\n\n\n¿Qué es un editor?\nUn editor es un community manager con un nombre poético. Está ahí para abogar por aquello que quiera publicar. Un editor es también un representante de un libro y, así como lo hacen representantes intentando llevar al estrellato a sus clientes, los editores acompañan a sus libros para que brillen en la publicación. Son la cara de una obra cuando aún no pueden sostenerse por sí mismos y son las manos tras bambalinas que alientan sus obras a llegar al éxito. \n",
<<<<<<< HEAD
  id: 5
=======
  id: 6
>>>>>>> 4baaaf547d630beb6b27e7800296c55f150d90aa
});

// Builds reference data (maybe not necessary for us, to check)
var store = [{
  "title": "404: Page not found",
  "author": null,
  "layout": "page",
  "link": "/404.html",
}
,{
<<<<<<< HEAD
=======
  "title": "Mateo-A Caballero",
  "author": null,
  "layout": "page",
  "link": "/_pages/Rese%F1a%20biografica/",
}
,{
>>>>>>> 4baaaf547d630beb6b27e7800296c55f150d90aa
  "title": null,
  "author": null,
  "layout": "default",
  "link": "/",
}
,{
  "title": "Search",
  "author": null,
  "layout": "page",
  "link": "/_pages/search/",
}
,{
  "title": null,
  "author": null,
  "layout": null,
  "link": "/assets/js/search.js",
}
,{
  "title": null,
  "author": null,
  "layout": null,
  "link": "/assets/css/style.css",
}
,{
  "title": "Valentina Osorio-Beltran",
  "author": null,
  "layout": "page",
  "link": "/_pages/valentina/",
}
]

// Query
var qd = {}; // Gets values from the URL
location.search.substr(1).split("&").forEach(function(item) {
    var s = item.split("="),
        k = s[0],
        v = s[1] && decodeURIComponent(s[1]);
    (k in qd) ? qd[k].push(v) : qd[k] = [v]
});

function doSearch() {
  var resultdiv = document.querySelector('#results');
  var query = document.querySelector('input#search').value;

  // The search is then launched on the index built with Lunr
  var result = index.search(query);
  resultdiv.innerHTML = "";
  if (result.length == 0) {    
    resultdiv.append(document.createElement('p').innerHTML = 'No results found.');
  } else if (result.length == 1) {
    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' result');
  } else {
    resultdiv.append(document.createElement('p').innerHTML = 'Found '+result.length+' results');
  }
  // Loop through, match, and add results
  for (var item in result) {
    var ref = result[item].ref;
    res = document.createElement('div')
    res.classList.add("result")
    link = document.createElement('a')
<<<<<<< HEAD
    link.setAttribute('href', '/mith301-project'+store[ref].link+'?q='+query)
=======
    link.setAttribute('href', '/proyecto-final'+store[ref].link+'?q='+query)
>>>>>>> 4baaaf547d630beb6b27e7800296c55f150d90aa
    link.innerHTML = store[ref].title || 'Untitled page'
    p = document.createElement('p')
    p.appendChild(link)
    res.appendChild(p)
    resultdiv.appendChild(res)
  }
}

var callback = function(){
  searchInput = document.querySelector('input#search') 
  if (qd.q) {
    searchInput.value = qd.q[0];
    doSearch();
  }
  searchInput.addEventListener('keyup', doSearch);
};

if (
    document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
  callback();
} else {
  document.addEventListener("DOMContentLoaded", callback);
}
